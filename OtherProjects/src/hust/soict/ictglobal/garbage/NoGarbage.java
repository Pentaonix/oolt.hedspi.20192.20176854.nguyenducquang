package hust.soict.ictglobal.garbage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class NoGarbage {
	
	private File source;
	
	public NoGarbage(String filename) {
		source = new File(filename);
	}
	
	public void execute() {
		Scanner reader;
		try {
			reader = new Scanner(this.source);
			StringBuffer str = new StringBuffer("");
			long start = System.currentTimeMillis();
			
			while (reader.hasNextLine()) {
				str.append(reader.nextLine());
				System.out.println(str);
			}
			
			System.out.println(System.currentTimeMillis() - start);
			reader.close();
			
		} catch (FileNotFoundException e) {
			System.out.println(e.toString());
		}
	}

	public static void main(String[] args) {
		NoGarbage demo = new NoGarbage("D:\\OOLT.HEDSPI.20192.20176745.HadoManh\\OtherProjects\\src\\hust\\soict\\ictglobal\\garbage\\something.txt");
		demo.execute();

	}

}
