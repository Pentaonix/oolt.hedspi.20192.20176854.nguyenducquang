package hust.soict.ictglobal.lab02;

public class MyMatrix {
	
	private final int ROW = 2, COLUMN = 5;
	
	private int[][] element;
	
	public MyMatrix() {
		this.element = new int[ROW][COLUMN];
	}
	
	public MyMatrix(int[][] element) {
		super();
		this.element = element;
	}

	public void traverse() {
		
		for (int i = 0; i < ROW; i++) {
			for (int j = 0; j < COLUMN; j++) {
				System.out.print(element[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	public MyMatrix add(MyMatrix toAdd) {
		
		MyMatrix result = new MyMatrix();
		
		for (int i = 0; i < ROW; i++) {
			for (int j = 0; j < COLUMN; j++) {
				result.element[i][j] = this.element[i][j] + toAdd.element[i][j];
			}
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		MyMatrix demo = new MyMatrix(new int[][] {
			{1,2,3,4,5},
			{6,7,8,9,10}
		});
		
		demo.add(new MyMatrix(new int[][] {
			{11,22,33,44,55},
			{66,77,88,99,100}
		})).traverse();;
	}
	

}
