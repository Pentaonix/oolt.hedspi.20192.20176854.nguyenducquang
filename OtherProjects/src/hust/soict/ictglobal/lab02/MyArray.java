package hust.soict.ictglobal.lab02;

public class MyArray {

	private int[] arr;

	public int[] getArr() {
		return arr;
	}

	public void setArr(int[] arr) {
		this.arr = arr;
	}

	public MyArray(int[] arr) {
		super();
		this.arr = arr;
	}
	
	public void traverse() {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	public void sort() {
		int n = arr.length;
		for (int i = 0; i < n - 1; i++)
			for (int j = 0; j < n - i - 1; j++)
				if (arr[j] > arr[j + 1]) {
					int temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}
	}
	
	public int total() {
		int total = 0;
		for (int i = 0; i < arr.length; i++) {
			total += arr[i];
		}
		return total;
	}
	
	public double average() {
		return this.total() / this.arr.length;
	}

	public static void main(String[] args) {
		MyArray demo = new MyArray(new int[] {64, 34, 25, 12, 22, 11, 90});
		
		System.out.print("Before: ");
		demo.traverse();
		
		demo.sort();
		
		System.out.print("After: ");
		demo.traverse();
		
		System.out.println("Total: " + demo.total());
		System.out.println("Average: " + demo.average());

	}

}
