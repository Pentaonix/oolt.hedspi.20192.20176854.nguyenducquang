package hust.soict.ictglobal.lab02;

import java.util.Scanner;

public class Month {

	private int month;
	private Scanner keyboard = new Scanner(System.in);

	public int getMonth() {
		return month;
	}

	public void setMonth() {
		do {
			System.out.print("Month: ");
			this.month = this.keyboard.nextInt();
			this.keyboard.nextLine();
		} while (this.month > 12 || this.month < 1);

	}

	public Month() {

	}

	public int getNumberOfDays() {
		switch (this.month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;

		case 4:
		case 6:
		case 9:
		case 11:
			return 30;

		default:
			String isLeapYear;

			do {
				System.out.print("Is leap year (y/n): ");
				isLeapYear = this.keyboard.nextLine();
				isLeapYear = isLeapYear.toLowerCase();
			} while (!isLeapYear.equals("y") && !isLeapYear.equals("n"));

			if (isLeapYear.equals("y"))
				return 29;
			return 28;

		}
		
	}
	
	public void done() {
		this.keyboard.close();
	}

	public static void main(String[] args) {
		Month demo = new Month();
		demo.setMonth();
		System.out.println(demo.getNumberOfDays());
		demo.done();
	}

}
