package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.List;

public class CompactDisc extends Disc implements Playable {

	private String artist;

	private List<Track> tracks = new ArrayList<Track>();

	public CompactDisc(int id, String title, String category, String director, float cost, String artist, List<Track> tracks) {
		super(id, title, category, director, cost);
		this.artist = artist;
		super.setLength(this.getLength());
		this.tracks = tracks;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	@Override
	public int getLength() {
		int length = 0;

		for (Track item : tracks) {
			length += item.getLength();
		}

		return length;
	}

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	public boolean addTrack(Track toAdd) {
		for (Track item : tracks) {
			if (toAdd.equals(item)) {
				return false;
			}
		}

		tracks.add(toAdd);
		return true;

	}

	public boolean removeTrack(Track toRemove) {
		return tracks.remove(toRemove);
	}

	@Override
	public void play() {
		tracks.forEach(Track::play);
	}
	@Override
	public int compareTo(CompactDisc toCompare) {
		return super.getTitle().compareTo(toCompare.getTitle());
	}

}
