package hust.soict.ictglobal.aims.media;

public class DigitalVideoDisc extends Disc implements Playable {
	
	public boolean search(String title) {
		return this.getTitle().toLowerCase().contains(title.toLowerCase());
	}

	public DigitalVideoDisc(int id, String title) {
		super();
		this.setId(id);
		this.setTitle(title);
	}

	public DigitalVideoDisc(int id, String title, String category) {
		super();
		this.setId(id);
		this.setTitle(title);
		this.setCategory(category);
	}

	public DigitalVideoDisc(int id, String title, String category, String director) {
		super();
		this.setId(id);
		this.setTitle(title);
		this.setCategory(category);
		setDirector(director);
	}

	public DigitalVideoDisc(int id, String title, String category, String director, int length, float cost) {
		super(id, title, category, director, length, cost);
	}

	@Override
	public String toString() {
		return "DigitalVideoDisc [director=" + getDirector() + ", length=" + getLength() + ", id=" + getId()
				+ ", title=" + getTitle() + ", category=" + getCategory() + ", cost=" + getCost() + "]";
	}

	@Override
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
		
	}
	@Override
	public int compareTo(DigitalVideoDisc toCompare) {
		return Math.round(this.getCost() - toCompare.getCost());
	}
	
}
